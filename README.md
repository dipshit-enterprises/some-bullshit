# some bullshit

android shenanigans 

to use this vendor, add this repo manifest:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
	<remote name="gitlab" fetch="https://gitlab.com/" />

	<project path="vendor/dipshit_enterprises" name="dipshit-enterprises/some-bullshit" remote="gitlab" revision="main" />
	<project path="vendor/dipshit_enterprises/BloatInstaller" name="dipshit-enterprises/bloat-installer" remote="gitlab" revision="main" />
</manifest>
```

``dipshit.mk`` also needs to be included somewhere. to do that you can add the following to a shell script that runs before starting the build:
```sh
cat << 'EOF' >> vendor/lineage/config/common.mk
$(call inherit-product, vendor/dipshit_enterprises/products/dipshit.mk)
EOF
```
